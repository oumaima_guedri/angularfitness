import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Tarif } from '../models/tarif.model';

@Injectable({
  providedIn: 'root'
})
export class TarifService {
  private baseUrl = 'http://localhost:8087/tarif';

  constructor(private http:HttpClient) { }

  getAll()
   {
     return this.http.get(this.baseUrl + '/all');
   }

   addtarif(t:Tarif):Observable<object>{
    return this.http.post("http://localhost:8087/tarif" ,t ).pipe()

  }

  getTarifById(id: number): Observable<any>
  {
    return this.http.get(this.baseUrl  + id);
  }

  supprimer(id: number): Observable<any>
  {
    return this.http.delete(this.baseUrl +  "/" +id, { responseType: 'text' });
  }
   update(ta:Tarif): any
   {
     return this.http.put(this.baseUrl ,ta).pipe();
   }
}
