import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListabonneComponent } from './listabonne.component';

describe('ListabonneComponent', () => {
  let component: ListabonneComponent;
  let fixture: ComponentFixture<ListabonneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListabonneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListabonneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
