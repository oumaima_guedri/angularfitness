import { FormGroup, FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditespaceRoutingModule } from './editespace-routing.module';
import { EditespaceComponent } from './editespace.component';


@NgModule({
  declarations: [
    EditespaceComponent
  ],
  imports: [
    CommonModule,
    EditespaceRoutingModule,
    FormsModule
  ]
})
export class EditespaceModule { }
