import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListespaceRoutingModule } from './listespace-routing.module';
import { ListespaceComponent } from './listespace.component';


@NgModule({
  declarations: [
    ListespaceComponent
  ],
  imports: [
    CommonModule,
    ListespaceRoutingModule
  ]
})
export class ListespaceModule { }
