import { EspaceService } from './../../../../service/espace.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addespace',
  templateUrl: './addespace.component.html',
  styleUrls: ['./addespace.component.css']
})
export class AddespaceComponent implements OnInit {
  url:any ="";


  esp: any={'nom':'','description':'', 'image':""};
  constructor( private service:EspaceService , private router:Router)
  { }

  ngOnInit(): void {
  }

  add(){
    console.log(this.esp);
    this.service.addespace(this.esp).subscribe({

       next: (data:any)=>{
         this.router.navigate (['admin/listespace'])

      },

      error: (e:any)=> console.error(e),

      complete:()=>{}

      })


}

}
