import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListeuserRoutingModule } from './listeuser-routing.module';
import { ListeuserComponent } from './listeuser.component';


@NgModule({
  declarations: [
    ListeuserComponent
  ],
  imports: [
    CommonModule,
    ListeuserRoutingModule
  ]
})
export class ListeuserModule { }
