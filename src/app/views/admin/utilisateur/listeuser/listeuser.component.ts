import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { UserCRUDService } from 'src/app/service/user-crud.service';

@Component({
  selector: 'app-listeuser',
  templateUrl: './listeuser.component.html',
  styleUrls: ['./listeuser.component.css']
})
export class ListeuserComponent implements OnInit {
  list: any;

  constructor(private serviceuser:UserCRUDService,private router:Router) { }

  ngOnInit(): void {
    this.getall();
    this.deleteUser;
  }


  getall():void{

    this.serviceuser.getAll().subscribe({next: (data) => {

    this.list= data;

    console.log(data);

    },

    error: (e) => console.error(e)

    });

      }
      deleteUser(id:number){
        this.serviceuser. supprimer(id)
          .subscribe(data => {
            this.deleteUser=data;
            console.log(data);
          },
            error => console.log(error));

      }
      update(esp:any){
        this.router.navigate(['admin/edituser' ,esp]);
        let navigationExtras:NavigationExtras={

          queryParams:{

          special:JSON.stringify(esp)

          }

          }

          this.router.navigate(['admin/edituser'],navigationExtras);

      }


}
