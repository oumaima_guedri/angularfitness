import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddeventsComponent } from './addevents/addevents.component';

const routes: Routes = [
  {path:'',component:AddeventsComponent}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddeventsRoutingModule { }
