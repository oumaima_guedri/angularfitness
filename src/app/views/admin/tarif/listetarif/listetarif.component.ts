import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { TarifService } from 'src/app/service/tarif.service';

@Component({
  selector: 'app-listetarif',
  templateUrl: './listetarif.component.html',
  styleUrls: ['./listetarif.component.css']
})
export class ListetarifComponent implements OnInit {

  listetarif: any;

  constructor(private servicetarif : TarifService, private router : Router) { }

  ngOnInit(): void {
    this.getall();
    this.deleteTarif;
  }

  getall():void{

    this.servicetarif.getAll().subscribe({next: (data) => {

    this.listetarif= data;

    console.log(data);

    },

    error: (t) => console.error(t)

    });

      }
      deleteTarif(id:number){
        this.servicetarif. supprimer(id)
          .subscribe(data => {
            this.deleteTarif=data;
            console.log(data);
          },
            error => console.log(error));

      }
      update(ta:any){
        this.router.navigate(['admin/edittarif' ,ta]);
        let navigationExtras:NavigationExtras={

          queryParams:{

          special:JSON.stringify(ta)

          }

          }

          this.router.navigate(['admin/edittarif'],navigationExtras);

      }
}
