import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActiviteCrudService } from 'src/app/service/activite-crud.service';

@Component({
  selector: 'app-addact',
  templateUrl: './addact.component.html',
  styleUrls: ['./addact.component.css']
})
export class AddactComponent implements OnInit {
  act: any={'nomA':'','DescriptionA':'', 'imageA':""};

  constructor(private serviceact:ActiviteCrudService,private router:Router) { }

  ngOnInit(): void {
  }

  add(){
    console.log(this.act)
    this.serviceact.addact(this.act).subscribe({

       next: (data:any)=>{
         this.router.navigate (['admin/listeact'])

      },

      error: (a:any)=> console.error(a),

      complete:()=>{}

      })


}

}
