import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddactComponent } from './addact.component';

const routes: Routes = [
  {path:'',component:AddactComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddactRoutingModule { }
