import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminloginRoutingModule } from './adminlogin-routing.module';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AdminloginComponent
  ],
  imports: [
    CommonModule,
    AdminloginRoutingModule,
    FormsModule
  ]
})
export class AdminloginModule { }
