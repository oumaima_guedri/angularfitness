import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditcoachComponent } from './editcoach.component';

describe('EditcoachComponent', () => {
  let component: EditcoachComponent;
  let fixture: ComponentFixture<EditcoachComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditcoachComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditcoachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
