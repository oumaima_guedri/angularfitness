import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { CoachService } from 'src/app/service/coach.service';

@Component({
  selector: 'app-listcoach',
  templateUrl: './listcoach.component.html',
  styleUrls: ['./listcoach.component.css']
})
export class ListcoachComponent implements OnInit {
  list:any;
  constructor(private service:CoachService ,private router:Router) { }

  ngOnInit(): void {
    this.getall();
    this.deleteCoach;
  }


  getall():void{

    this.service.getAll().subscribe({next: (data) => {

    this.list= data;

    console.log(data);

    },

    error: (e) => console.error(e)

    });

      }
      deleteCoach(id:number){
        this.service. supprimer(id)
          .subscribe(data => {
            this.deleteCoach=data;
            console.log(data);
          },
            error => console.log(error));

      }
      update(coa:any){
        this.router.navigate(['admin/editespace' ,coa]);
        let navigationExtras:NavigationExtras={

          queryParams:{

          special:JSON.stringify(coa)

          }

          }

          this.router.navigate(['admin/editcoach'],navigationExtras);

      }

}
