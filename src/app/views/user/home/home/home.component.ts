import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActiviteCrudService } from 'src/app/service/activite-crud.service';
import { TarifService } from 'src/app/service/tarif.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  listeact: any;
  listetarif: any;

  constructor(private serviceact:ActiviteCrudService,private router:Router,private servicetarif : TarifService) { }

  ngOnInit(): void {
    this.getall();
    this.getalltarif();
  }

  getall():void{

    this.serviceact.getAll().subscribe({next: (data) => {
    
    this.listeact= data;
    
    console.log(data);
    
    },
    
    error: (a) => console.error(a)
    
    });
    
      }

      getalltarif():void{

        this.servicetarif.getAll().subscribe({next: (data) => {
    
        this.listetarif= data;
    
        console.log(data);
    
        },
    
        error: (t) => console.error(t)
    
        });
    
          }


}
